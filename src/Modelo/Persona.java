/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import lombok.Getter;
import lombok.Setter;

// NO OLVIDAR Añadir la libreria lombok para evitar errores
@Getter
@Setter
public class Persona {

    private int id;
    private String nombre;
    private String apellido;
    private String direccion;
    private String cedula;
    private String email;
}
