/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author macbook
 */
@Getter
@Setter
public class Producto {

    private int  idProducto;
    private String nombreProducto;
    private String marca;
    private String precio;
    private String descripcion;

}
