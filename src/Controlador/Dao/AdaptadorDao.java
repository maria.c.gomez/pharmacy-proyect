/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Controlador.Dao.Conexion;
import Controlador.Dao.InterfazDao;
import Controlador.Lista.ListaSimple;
import java.io.File;
import java.io.FileOutputStream;

/**
 *
 * @author macbook
 */
public class AdaptadorDao<T> implements InterfazDao<T>{
    
    private Conexion conexion;
    private Class<T> clazz;
    private String data_dir;

    public AdaptadorDao(Class<T> clazz) {
        this.conexion = new Conexion();
        this.clazz = clazz;
        data_dir = Conexion.REPOSITORIO + File.separatorChar+clazz.getSimpleName()+".json";
    }
        
    
    @Override
    public void guardar(T dato) throws Exception {
        this.conexion.getxStream().toXML(dato, new FileOutputStream(data_dir));
    }

    @Override
    public ListaSimple<T> listar() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void modificar(T dato) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public T obtener(Integer id) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}

