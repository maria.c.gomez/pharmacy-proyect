/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Dao;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;

/**
 *
 * @author macbook
 */

public class Conexion {
    public static final String REPOSITORIO = "datos";
    private XStream xStream;
    public Conexion() {
        xStream = new XStream(new JettisonMappedXmlDriver());//DomDriver
        //xStream = new XStream(new DomDriver());
        xStream.setMode(XStream.NO_REFERENCES);
    }

    public XStream getxStream() {
        return xStream;
    }
    
}
