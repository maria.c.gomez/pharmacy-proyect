/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador.Lista;

/**
 *
 * @author macbook
 */

public class ListaSimple<T> {

    public NodoDato cabecera;

    public ListaSimple() {
        this.cabecera = null;
    }

    public boolean estaVacio() {
        return (this.cabecera == null);
    }

    public void imprimir() {
        if (!estaVacio()) {
            NodoDato tmp = cabecera;//
            System.out.println("------");
            while (tmp != null) {
                System.out.print(tmp.getDato() + "\t");
                tmp = tmp.getSiguiente();
            }
            System.out.println("------");
        }
    }

    private void insertar(T dato) {
        NodoDato tmp = new NodoDato(dato, cabecera);
        cabecera = tmp;
    }

    public boolean insertarDato(T dato) {
        try {
            inserTarFinal(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public T extraer() {
        T dato = null;
        if (!estaVacio()) {
            dato = (T) cabecera.getDato();
            cabecera = cabecera.getSiguiente();
        }
        return dato;
    }

    public T consultarDatoPos(int pos) {
        T dato = null;
        if (!estaVacio() && (pos <= tamanio() - 1)) {
            NodoDato tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                dato = (T) tmp.getDato();
            }
        }
        return dato;
    }

    public boolean modificarDatoPos(int pos, T data) {

        if (!estaVacio() && (pos <= tamanio() - 1)) {
            NodoDato tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                tmp.setDato(data);
                return true;
            }
        }
        return false;
    }

    public int tamanio() {
        int tamanio = 0;
        if (!estaVacio()) {
            NodoDato tmp = cabecera;
            while (tmp != null) {
                tamanio++;
                tmp = tmp.getSiguiente();
            }
        }
        return tamanio;
    }

    public void insertar(T dato, int pos) {
        if (estaVacio() || pos < 0) {
            insertar(dato);
        } else {
            NodoDato iterador = cabecera;
            for (int i = 0; i < pos; i++) {
                if (iterador.getSiguiente() == null) {
                    break;
                }
                iterador = iterador.getSiguiente();
            }
            NodoDato tmp = new NodoDato(dato, iterador.getSiguiente());
            iterador.setSiguiente(tmp);

        }
    }

    private void inserTarFinal(T dato) {
        insertar(dato, tamanio() - 1);
    }

    public void burbujaMejorado() {
        if (!estaVacio()) {
            T dato = consultarDatoPos(0);
            if (dato instanceof Number) {
                int l = 1, r = tamanio() - 1, k = tamanio() - 1;
                do {
                    for (int j = r; j >= 1; j--) {
                        Number x = (Number) consultarDatoPos(j - 1);
                        Number y = (Number) consultarDatoPos(j);
                        if (x.doubleValue() > y.doubleValue()) {
                            T t = consultarDatoPos(j - 1);
                            modificarDatoPos((j - 1), consultarDatoPos(j));// = datos[j];
                            modificarDatoPos((j), t);//datos[j] = t;
                            k = j;

                        }

                    }
                    l = k + 1;
                    for (int j = l; j <= r; j++) {

                        //  int x = (Integer) consultarDatoPos(j - 1);
                        // int y = (Integer) consultarDatoPos(j);
                        Number x = (Number) consultarDatoPos(j - 1);
                        Number y = (Number) consultarDatoPos(j);

                        if (x.doubleValue() > y.doubleValue()) {//if (x > y) {
                            T t = consultarDatoPos(j - 1);
                            modificarDatoPos((j - 1), consultarDatoPos(j));// = datos[j];
                            modificarDatoPos((j), t);//datos[j] = t;
                            k = j;

                        }

                    }
                    r = k - 1;
                } while (l < r);
            }
        }
    }

}
